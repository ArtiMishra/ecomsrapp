package com.nt.mishra.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="coupon_tab")
public class Coupon {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="coup_id_col")
	private Long id;
	
	@Column(name="coup_code_col")
	private String code;
	
	@Column(name="coup_note_col")
	private String note;
	
	@Column(name="coup_per_col")
	private Double percentage;
	
	@Column(name="coup_avail_col")
	private String available;
	
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	@Temporal(TemporalType.DATE)
	@Column(name="coup_exdate_col")
	private Date expDate;
	
	@Column(name="coup_talow_col")
	private Double totalAllow;
	

}
