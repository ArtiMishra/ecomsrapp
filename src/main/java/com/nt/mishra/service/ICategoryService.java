package com.nt.mishra.service;

import java.util.List;

import com.nt.mishra.entity.Category;

public interface ICategoryService {
	
	Long saveCategory(Category category);
	void updateCategory(Category category);
	void deleteCategory(Long id);
	Category getOneCategory(Long id);
	List<Category> getAllCategorys();


}
