package com.nt.mishra.service;

import java.util.List;

import com.nt.mishra.entity.Brand;

public interface IBrandService {
	
	Long saveBrand(Brand brand);
	void updateBrand(Brand brand);
	void deleteBrand(Long id);
	Brand getOneBrand(Long id);
	List<Brand> getAllBrands();


}
