package com.nt.mishra.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nt.mishra.entity.Category;
import com.nt.mishra.repo.ICategoryRepository;
import com.nt.mishra.service.ICategoryService;

@Service
public class CategoryServiceImpl implements ICategoryService {
	
	@Autowired
	private ICategoryRepository repo;

	@Override
	@Transactional
	public Long saveCategory(Category category) {
		return repo.save(category).getId();
	}

	@Override
	@Transactional
	public void updateCategory(Category category) {
		repo.save(category);
	}

	@Override
	@Transactional
	public void deleteCategory(Long id) {
		repo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Category getOneCategory(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Category> getAllCategorys() {
		return repo.findAll();
	}


}
