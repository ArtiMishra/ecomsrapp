package com.nt.mishra.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nt.mishra.entity.CategoryType;
import com.nt.mishra.repo.ICategoryTypeRepository;
import com.nt.mishra.service.ICategoryTypeService;
import com.nt.mishra.util.AppUtil;

@Service
public class CategoryTypeServiceImpl implements ICategoryTypeService {
	
	@Autowired
	private ICategoryTypeRepository repo;

	@Override
	@Transactional
	public Long saveCategoryType(CategoryType categorytype) {
		return repo.save(categorytype).getId();
	}

	@Override
	@Transactional
	public void updateCategoryType(CategoryType categorytype) {
		repo.save(categorytype);
	}

	@Override
	@Transactional
	public void deleteCategoryType(Long id) {
		repo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public CategoryType getOneCategoryType(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<CategoryType> getAllCategoryTypes() {
		return repo.findAll();
	}
	
	public Map<Integer, String> getCategoryTypeIdAndName() {
			List<Object[]>  list = repo.getCategoryTypeIdAndName();
			return AppUtil.convertListToMap(list);
		   }	


}
