package com.nt.mishra.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nt.mishra.entity.Brand;
import com.nt.mishra.repo.IBrandRepository;
import com.nt.mishra.service.IBrandService;


@Service
public class BrandServiceImpl implements IBrandService {
	
	@Autowired
	private IBrandRepository repo;

	@Override
	@Transactional
	public Long saveBrand(Brand brand) {
		return repo.save(brand).getId();
	}

	@Override
	@Transactional
	public void updateBrand(Brand brand) {
		repo.save(brand);
	}

	@Override
	@Transactional
	public void deleteBrand(Long id) {
		repo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Brand getOneBrand(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Brand> getAllBrands() {
		return repo.findAll();
	}


}
