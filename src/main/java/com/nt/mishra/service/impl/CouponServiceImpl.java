package com.nt.mishra.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nt.mishra.entity.Coupon;
import com.nt.mishra.repo.ICouponRepository;
import com.nt.mishra.service.ICouponService;

@Service
public class CouponServiceImpl implements ICouponService {
	
	@Autowired
	private ICouponRepository repo;

	@Override
	@Transactional
	public Long saveCoupon(Coupon coupon) {
		return repo.save(coupon).getId();
	}

	@Override
	@Transactional
	public void updateCoupon(Coupon coupon) {
		repo.save(coupon);
	}

	@Override
	@Transactional
	public void deleteCoupon(Long id) {
		repo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Coupon getOneCoupon(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Coupon> getAllCoupons() {
		return repo.findAll();
	}


}
