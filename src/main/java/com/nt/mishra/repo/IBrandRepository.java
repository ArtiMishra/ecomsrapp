package com.nt.mishra.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nt.mishra.entity.Brand;


public interface IBrandRepository extends JpaRepository<Brand, Long> {

}
