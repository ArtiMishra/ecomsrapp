package com.nt.mishra.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nt.mishra.entity.Category;

public interface ICategoryRepository extends JpaRepository<Category, Long> {

}
