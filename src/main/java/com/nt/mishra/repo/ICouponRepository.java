package com.nt.mishra.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nt.mishra.entity.Coupon;

public interface ICouponRepository extends JpaRepository<Coupon, Long> {

}
