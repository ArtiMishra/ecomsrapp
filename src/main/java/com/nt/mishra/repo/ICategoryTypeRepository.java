package com.nt.mishra.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nt.mishra.entity.CategoryType;

public interface ICategoryTypeRepository extends JpaRepository<CategoryType, Long> {

	@Query("SELECT id, name FROM CategoryType")
	  List<Object[]> getCategoryTypeIdAndName();
	
}
